﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Controllers
{
    public class CaptchaController : Controller
    {
        // GET: Captcha
        public FileResult Index()
        {
			Session["CaptchaText"] = GetRandomText();
		    MemoryStream memoryStream = CaptchaHelper.GetCaptchaImage(Session["CaptchaText"].ToString());
			return File(memoryStream.ToArray(), "image/png");
        }


		private static string GetRandomText()
		{
			StringBuilder randomText = new StringBuilder();
			string alphabets = "012345679ACEFGHKLMNPRSWXZabcdefghijkhlmnopqrstuvwxyz";
			Random r = new Random();
			for (int j = 0; j <= 5; j++)
			{
				randomText.Append(alphabets[r.Next(alphabets.Length)]);
			}
			return randomText.ToString();
		}

	}
}
﻿using Blogat.Commands.Contact;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Controllers
{
    public class ContactController : BaseController
	{

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
		public ActionResult Index(AddMessageCommand Command)
        {
            if (ModelState.IsValid)
            {

                DAL.Message guestMessage = new Message()
                {
                    Name = Command.Name,
                    Email = Command.Email,
                    Text = Command.Message,
                    SubmittedAt = DateTime.UtcNow,
                    Tel = Command.Tel,
                    UserId = IsAuthenticated ? (int?)LoggedUser.Id : null
                };

                using (Context)
                {
                    Context.MessageRepository.Add(guestMessage);
                }
            }
			return RedirectToAction("Index");
		}

	}
}
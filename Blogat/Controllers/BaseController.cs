﻿using Common.Security;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Controllers
{
    public class BaseController : Controller
    {
        public readonly UnitOfWork Context;


		protected BlogatIdentity LoggedUser
		{
			get => HttpContext?.User?.Identity as BlogatIdentity;
		}

		protected bool IsAuthenticated
		{
			get => User?.Identity?.IsAuthenticated ?? false;
		}

        public BaseController()
        {
            Context = new UnitOfWork();
        }
        
    }
}
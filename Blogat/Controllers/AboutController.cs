﻿using Blogat.Commands.About;
using Blogat.Models.About;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Controllers
{
    public class AboutController : BaseController
	{
		[HttpGet]
		public ActionResult Index()
        {
			AboutModel Model = new AboutModel();
			using (Context)
			{

				Model.Text = Context.AboutMeRepository.FirstOrDefault(x => true)?.Text;

			}

            return View(Model);
        }

		[HttpGet]
		public ActionResult Edit()
		{
			AboutModel Model = new AboutModel();
			using (Context)
			{

				Model.Text = Context.AboutMeRepository.FirstOrDefault(x => true)?.Text;

			}

			return View(Model);
		}

		[HttpPost]
		public RedirectToRouteResult Edit(EditAboutCommand Command)
		{
            if(ModelState.IsValid)
            {

                using (Context)
                {

                    Context.AboutMeRepository.FirstOrDefault(x => true).Text = Command.Text;

                }
            }

			return RedirectToAction("Index");
		}


	}
}
﻿using Blogat.Commands.Post;
using Blogat.Models.Post;
using Common.Enums;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Controllers
{
	public class PostController : BaseController
	{
		[HttpGet]
		public ActionResult Index(int PostId)
		{
			PostViewModel Model = GetPostViewModelById(PostId);

			return View(Model);
        }


		[HttpGet]
		[Authorize(Roles = "Admin")]
		public ActionResult Edit(int PostId)
		{
			PostViewModel Model = GetPostViewModelById(PostId);

			return View(Model);
		}

		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult Edit(EditPostCommand Command)
		{
            if(ModelState.IsValid)
            {

			    using (Context)
			    {
				    Post dalPost = Context.PostRepository.Get(Command.PostId);
				    dalPost.Title = Command.PostTitle;
				    dalPost.Text = Command.PostContent;
			    }

            }
            return RedirectToAction("Index", new { Command.PostId });
		}


		[HttpPost]
		[Authorize(Roles = "User,Admin")]
		public ActionResult AddComment(AddCommentCommand Command)
        {
            if (ModelState.IsValid)
            {
                DAL.Comment dalComment = new Comment();
                dalComment.CreatedAt = DateTime.UtcNow;
                dalComment.Text = Command.CommentContent;

                using (Context)
                {

                    int? authorId = Context.BlogatUserRepository.FirstOrDefault(x => x.Id == LoggedUser.Id)?.Id;
                    dalComment.AuthorId = authorId;

                    DAL.Post dalPost = Context.PostRepository.Get(Command.PostId);
                    dalPost
                        .Comment
                        .Add(dalComment);
                }
            }


			return RedirectToAction("Index", new { Command.PostId });
		}

		private PostViewModel GetPostViewModelById(int Id)
		{
			PostModel Post = new PostModel();
			PostViewModel Model = new PostViewModel();
			using (Context)
            {
                bool IsAdmin = User.IsInRole("Admin");
                Post dalPost = Context.PostRepository.FirstOrDefault(x => x.Id == Id &&
                (x.Topic.Status == (int)TopicStatus.Active || IsAdmin));
				List<CommentModel> comments = new List<CommentModel>();

				foreach (var dalComment in dalPost.Comment)
				{
					comments.Add(new CommentModel()
					{
						Author = dalComment.BlogatUser?.Nick ?? "Deleted user",
						CreatedAt = dalComment.CreatedAt,
						Text = dalComment.Text
					});

				}

				Model.TopicId = dalPost.TopicId;
				Model.TopicTitle = dalPost.Topic.Title;
				Post.PostId = dalPost.Id;
				Post.Comments = comments;
				Post.Content = new MvcHtmlString(dalPost.Text);
				Post.PostedAt = dalPost.CreationDate;
				Post.Title = dalPost.Title;
				Post.Settings = new PostSettingsModel()
				{
					Mode = CommentsMode.ToggleEnabled,
					IsEditable = true

				};
			}

			Model.Post = Post;

			return Model;
		}

	}
}
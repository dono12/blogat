﻿
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Blogat.Models.Dashboard;
using Blogat.Models.Topic;
using Common.Code;
using Common.Security;
using DAL;
using DAL.Extentions;
using DAL.UnitsOfWork;
using Common.Enums;
using Blogat.Commands.Dashboard;
using Blogat.Models.Shared;
using DAL.IRepository;

namespace Blogat.Controllers
{
	public class DashboardController : BaseController
	{
		[HttpGet]
		public ActionResult Index(int? PageNo, int PageSize = 5)
		{
			DashboardViewModel Model = new DashboardViewModel();
			List<TopicModel> Topics = new List<TopicModel>();
            PaginationModel Pagination = new PaginationModel();
            if (PageNo == null || PageNo < 1)
            {
                PageNo = 1;
            }
            Pagination.CurrentPage = (int)PageNo;
			using (Context)
			{
				bool IsAdmin = User.IsInRole("Admin");

				double AllTopicsCount = Context.TopicRepository.Count(
				   x => x.Status == (int)TopicStatus.Active || IsAdmin);

				Pagination.LastPage = (int)Math.Ceiling(AllTopicsCount / PageSize);
				if (Pagination.LastPage < Pagination.CurrentPage)
					Pagination.CurrentPage = Pagination.LastPage;


                IEnumerable<Topic> topics =
                    Context
                        .TopicRepository
						.OrderBy(x => x.Status)
						.ThenByDescending(x => x.CreatedAt)
						.GetPage(Pagination.CurrentPage, PageSize)
						.Where(x => x.Status == (int)TopicStatus.Active || IsAdmin);
                        

               

				Model.AllTags = getAllTags(Context);

				foreach (var topic in topics)
				{

                    Topics.Add(AutoMapper.Mapper.Map<TopicModel>(topic));
				}
			}

			Model.Topics = Topics;
			Model.Pagination = Pagination;

			return View(Model);
		}


		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult Index(AddTopicCommand Command)
		{
            if (ModelState.IsValid)
            {

                DAL.Topic dalTopic = new Topic();
				dalTopic.Title = Command.Title;
				List<string> tagsList = new List<string>(Command.Tags.Split(','));

				IEnumerable<DAL.Tag> inputTags = tagsList.Select(x => 
				new DAL.Tag() { TagName = x}
				);

				
				using (Context)
				{
					var allTags = Context.TagRepository.GetAll();

					var existingDalTags = allTags.Where(x => tagsList.Exists(y => y == x.TagName)).ToList();
					int existingCount = existingDalTags.Count;
					var newDalTags = inputTags.Where(x =>
					    existingCount > 0 ? !existingDalTags.Exists(y => y.TagName == x.TagName) : true).ToList();

					existingDalTags.AddRange(newDalTags);

					
					Context.TopicRepository.Add(
						new Topic()
						{
							Title = Command.Title,
							Status = 0,
							Tag = existingDalTags,
							CreatedAt = DateTime.UtcNow
								
						}
						);

                }
            }
            


			return RedirectToAction("Index");
		}

		[HttpGet]
		[Authorize(Roles = "Admin")]
		public ActionResult Edit(int TopicId)
		{
			EditTopicViewModel Model = new EditTopicViewModel();
			using (Context)
			{
				DAL.Topic topic = Context.TopicRepository.Get(TopicId);
				Model.AllTags = getAllTags(Context);
				
				Model.Topic = new TopicModel()
					{
						Title = topic.Title,
						TopicId = topic.Id,
						Tags = topic.Tag.Select(x => x.TagName).OrderBy(x => x)
					};
				
			}
			

			return View(Model);
		}


		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult Edit(EditTopicCommand Command)
        {
            if (ModelState.IsValid)
            {
				using (Context)
				{
					Topic dalTopic = Context.TopicRepository.Include(x => x.Tag).First(x => x.Id == Command.TopicId);
					dalTopic.Title = Command.Title;

					List<string> tagsList = new List<string>(Command.Tags.Split(','));

					IEnumerable<DAL.Tag> inputTags = tagsList.Select(x =>
					new DAL.Tag() { TagName = x }
					);


					var allTags = Context.TagRepository.GetAll();

					var existingDalTags = allTags.Where(x => tagsList.Exists(y => y == x.TagName)).ToList();
					int existingCount = existingDalTags.Count;
					var newDalTags = inputTags.Where(x =>
					 existingCount > 0 ? !existingDalTags.Exists(y => y.TagName == x.TagName) : true).ToList();

					existingDalTags.AddRange(newDalTags);
					dalTopic.Tag = existingDalTags;


                }
            }



            return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult Rate(int TopicId, byte Rate)
		{
			if (!User.Identity.IsAuthenticated)
				return Json(new { status = HttpStatusCode.Unauthorized }, JsonRequestBehavior.DenyGet);
			if (Rate < 1 || Rate > 5)
				return Json(new { status = HttpStatusCode.Forbidden }, JsonRequestBehavior.DenyGet);
			using (Context)
			{
				DAL.Topic topic = Context.TopicRepository.FirstOrDefault(x => x.Id == TopicId);
				if (topic == null)
					return Json(new { status = HttpStatusCode.NotFound }, JsonRequestBehavior.DenyGet);

				int id = LoggedUser.Id;
				BlogatUser user = Context.BlogatUserRepository.Get(id);
				DAL.Rate existingRate = user.Rate.FirstOrDefault(x => x.TopicId == TopicId);
				if (existingRate == null)
				{

					Rate rate = new Rate();
					rate.UserId = id;
					rate.TopicId = TopicId;
					rate.TopicRating = Rate;
					user.Rate.Add(rate);
				}
				else
				{
					existingRate.TopicRating = Rate;
				}
				Context.Complete();
				double? avaregeRate = topic.GetAvaregeRate();
				return Json(new { status = HttpStatusCode.OK, value = avaregeRate }, JsonRequestBehavior.DenyGet);
			}
		}

		[HttpGet]
		public JsonResult GetRatings(int[] TopicsIds)
		{
			if(TopicsIds == null)
				return Json(new { status = HttpStatusCode.BadRequest }, JsonRequestBehavior.AllowGet);
			using (Context)
			{
				IEnumerable<DAL.Topic> topics = null;
				List<RatingModel> model = new List<RatingModel>();

				try
				{
					topics = Context.TopicRepository.GetAllByKeys(TopicsIds);
					foreach (Topic topic in topics)
					{
						model.Add(new RatingModel()
						{
							TopicId = topic.Id,
							Rating = topic.GetAvaregeRate()
						});
					}
				}
				catch (Exception e)
				{
					return Json(new { status = HttpStatusCode.NotFound }, JsonRequestBehavior.AllowGet);
				}
				if (topics == null)
					return Json(new { status = HttpStatusCode.NotFound }, JsonRequestBehavior.AllowGet);

				
				return Json(new { status = HttpStatusCode.OK, value = model.ToArray() }, JsonRequestBehavior.AllowGet);
			}
		}


		[HttpGet]
		public RedirectToRouteResult ToggleStatus(int TopicId)
		{
			using (Context)
			{
				DAL.Topic topic = Context.TopicRepository.FirstOrDefault(x => x.Id == TopicId);
				if(topic != null)
					topic.Status ^= 1;
				return RedirectToAction("Index");
			}
		}

		[HttpPost]
		[Authorize(Roles = "Admin,User")]
		public JsonResult CanRate(int TopicId)
		{
			using (Context)
			{
				int id = LoggedUser.Id;
				BlogatUser user = Context.BlogatUserRepository.Get(id);
				if (user.Rate.FirstOrDefault(x => x.TopicId == TopicId) != null)
				{

					return Json(HttpStatusCode.Conflict, JsonRequestBehavior.DenyGet);
				}

			}
			return Json(HttpStatusCode.OK, JsonRequestBehavior.DenyGet);



		}

		private string getAllTags(UnitOfWork Ctx)
		{

			return Ctx.TagRepository
				.GetAll()
				.Select(x => x.TagName)
				.Aggregate((x, y) => x + "," + y);
		}
	}
}
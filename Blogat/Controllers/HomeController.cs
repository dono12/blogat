﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Controllers
{
    public class HomeController : BaseController
    {
        public RedirectToRouteResult Index()
        {
			return RedirectToAction("Index","Dashboard");

		}
	}
}
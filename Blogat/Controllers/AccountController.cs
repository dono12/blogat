﻿using Blogat.Models.Account;
using Common.Enums;
using Common.Helpers;
using Common.Helpers.Smtp;
using Common.Models;
using Common.Security;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Blogat.Controllers
{
    public class AccountController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        public ActionResult LogIn(string returnUrl)
        {
            if (IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult LogIn(LoginUserViewModel Model, string returnUrl)
        {
			if(Model.Captcha != Session["CaptchaText"].ToString())
				ModelState.AddModelError("Captcha", "Wrong Captcha.");

			if (ModelState.IsValid)
            {
                using (Context)
                {

                    BlogatUser user = Context.BlogatUserRepository.FirstOrDefault(u => u.Nick == Model.Nick);

                    bool passwordAccepted = false;
                    if (user != null)
                    {
                        //encypt provided password using user's password salt and compare with current pass
                        passwordAccepted = PasswordHelper.CheckHash(Model.Password, user.PasswordHash, user.PasswordSalt);

                        if (passwordAccepted)
                        {
                            //authenticate user and redirect to requested page
                            StartSession(user.Nick);
                            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }
                        }
						else
						{
							ModelState.AddModelError("PassError", "Wrong password.");
						}

					}
					else
					{
						ModelState.AddModelError("UserError", "User doesn't exist.");
					}

				}
			}

            return View();
        }
		
		[HttpGet]
		public ActionResult Register()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Register(RegisterUserViewModel Model)
		{

			if (Model.Captcha != Session["CaptchaText"].ToString())
				ModelState.AddModelError("Captcha", "Wrong Captcha.");

			if (ModelState.IsValid)
			{
				using (Context)
				{
					HashedModel hashedModel = PasswordHelper.GenerateHashWithSalt(Model.Password);
					Context.BlogatUserRepository.Add(
						new BlogatUser()
						{
							Email = Model.Email,
							Role = (int)UserRoles.User,
							Nick = Model.Nick,
							PasswordHash = hashedModel.Hash,
							PasswordSalt = hashedModel.Salt,
							IsActive = false

						}
						);
				}
				return RedirectToAction("RegistrationConfirmation");
			}
			return View();
		}

		[HttpGet]
		public ActionResult RegistrationConfirmation()
		{
			return View();
		}

		[HttpGet]
        [Authorize]
        public ActionResult LogOut()
        {
            //logout
            FormsAuthentication.SignOut();


            //go back to login page
            return RedirectToAction("Index","Dashboard");
        }

        [HttpGet]
        public ActionResult ChangePassword(string Guid)
        {
            BlogatUser user;
            BlogatIdentity identity = User.Identity as BlogatIdentity;
            ChangePasswordViewModel model = new ChangePasswordViewModel();
            using (Context)
            {
                user = Context.PasswordGuidRepository.FirstOrDefault(g => g.DynamicGuid == Guid)?.BlogatUser;
            }

            if (identity == null && user == null)
            {
                return LogOut();
            }
            else
            {
                model.Guid = Guid;
                return View(model);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (Context)
                {
                    BlogatUser user = Context.PasswordGuidRepository.FirstOrDefault(g => g.DynamicGuid == model.Guid)?.BlogatUser;

                    if (LoggedUser != null)
                        user = Context.BlogatUserRepository.FirstOrDefault(a => a.Nick.ToLower().Equals(LoggedUser.Login.ToLower()));

                    if (user != null)
                    {
                        HashedModel resoult = PasswordHelper.GenerateHashWithSalt(model.NewPassword);

                        user.PasswordHash = resoult.Hash;
                        user.PasswordSalt = resoult.Salt;

                        Context.PasswordGuidRepository.RemoveIf(g => g.DynamicGuid == model.Guid);

                    }
                    LogOut();
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            ForgotPasswordViewModel model = new ForgotPasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgotPassword(ForgotPasswordViewModel Model)
        {
            DAL.BlogatUser user;
            using (Context)
            {
                user = Context.BlogatUserRepository.FirstOrDefault(u => u.Email == Model.Email);
                if (user != null)
                {
                    PasswordGuid latestGuid = Context.PasswordGuidRepository.Find(g => g.BlogatUser.Id == user.Id)?.OrderByDescending(x => x.CreatedAt).FirstOrDefault();

                    TimeSpan OneDay = new TimeSpan(1, 0, 0, 0),
                        TenMinutes = new TimeSpan(0, 10, 0);

                    IEnumerable<PasswordGuid> ExpiredGuidsForUser = Context.PasswordGuidRepository
                        .Find(u => u.BlogatUser.Id == user.Id)
                        .Where(g => DateTime.UtcNow - g.CreatedAt > OneDay);

                    Context.PasswordGuidRepository.RemoveRange(ExpiredGuidsForUser);


                    if (latestGuid != null && DateTime.UtcNow - latestGuid.CreatedAt < TenMinutes)
                    {
                        ModelState.AddModelError("Time", "Link ze zmianą hasła może zostać wygenerowny jedynie co minimum 10 minut.");
                        return View();
                    }


                    string guid = Guid.NewGuid().ToString();

                    PasswordGuid passwordGuid = new PasswordGuid();
                    passwordGuid.DynamicGuid = guid;
                    passwordGuid.BlogatUser = user;
                    passwordGuid.CreatedAt = DateTime.UtcNow;

                    Context.PasswordGuidRepository.Add(passwordGuid);

                    string MessegeBody =
                        $"Witaj {user.Nick}\n" +
                        "Aby zmienić hasło wejdź na ten \n" +
                        $"<a href=\"{Url.Action("ChangePassword", "Account", new { Guid = guid }, "http")}\">Link</a>\n";


                    SmtpClientHelper.SendEmail(MessegeBody, "Zmiana hasła", new string[] { user.Email });

                }
                else
                {
                    ModelState.AddModelError("LoginError", "Taki email nie istnieje.");
                    return View();
                }
                return RedirectToAction("SendEmailConfirmation");
            }

        }

        //[Authorize(Roles = "Administrator, Moderator")]
		/// <summary>
		/// For testing only
		/// </summary>
		/// <param name="Password"></param>
		/// <returns></returns>
        public JsonResult GenerateHash(string Password)
        {
            var tuple = PasswordHelper.GenerateHashWithSalt(Password);
            return Json(new { PasswordHash = tuple.Hash, Salt = tuple.Salt }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult SendEmailConfirmation()
        {
            return View();
        }


        #region Private methods

        internal void StartSession(string nick, bool codeVerified = false)
        {
            //authenticate user and redirect to requested page
            FormsAuthentication.SetAuthCookie(nick, false);
        }

        /// <summary>
        /// Send email with new password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="name"></param>
        private void SendResetConfirmationEmail(BlogatUser User)
        {
            string messege = $"Witaj {User.Nick}\n" +
            $"Twoje hasło zostało zmienione\n" +
            $"Użyj swojego Nicku {User.Nick} aby się zalogować.";
            SmtpClientHelper.SendEmail(messege, "Zmiana hasła", new[] { User.Email });

        }


        #endregion


    }
    
}
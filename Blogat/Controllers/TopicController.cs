﻿using Blogat.Commands.Topic;
using Blogat.Models.Post;
using Blogat.Models.Topic;
using Common.Code;
using Common.Enums;
using Common.Helpers;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Controllers
{
	public class TopicController : BaseController
    {
		[HttpGet]
		public ActionResult Index(int TopicId)
		{
			TopicViewModel Model = new TopicViewModel();
			Model.Posts = new List<PostModel>();
			List<DAL.Post> Posts;

			Model.TopicId = TopicId;
			using (Context)
			{
                bool IsAdmin = User.IsInRole("Admin");
				DAL.Topic Topic = Context.TopicRepository.FirstOrDefault(x => x.Id == TopicId &&
                (x.Status == (int)TopicStatus.Active || IsAdmin));
                Posts = Topic.Post.OrderByDescending(x => x.CreationDate.Date).ToList();

				
				Model.TopicTitle = Topic.Title;

				foreach (var dalPost in Posts)
				{
					List<CommentModel> comments = new List<CommentModel>();

					foreach (var dalComment in dalPost.Comment)
					{
						comments.Add(new CommentModel()
						{
							Author = dalComment.BlogatUser?.Nick ?? "User Deleted",
							CreatedAt = dalComment.CreatedAt,
							Text = dalComment.Text
						});

					}

					
					TagBuilder spoiler = new TagBuilder("a");
					spoiler.InnerHtml = "...(continue reading)";
					spoiler.Attributes.Add("href", Url.Action("Index", "Post", new { PostId = dalPost.Id }));
					

					string shortText = FormatHelper.ShortenText(dalPost.Text, Consts.PostPreviewLength, false);
					MvcHtmlString content;
					if (shortText.Length < dalPost.Text.Length)
					{
						content = new MvcHtmlString(shortText + spoiler);
					}
					else
					{
						content = new MvcHtmlString(shortText);
					}


					Model.Posts.Add(new PostModel()
					{
						PostId = dalPost.Id,
						Comments = comments,
						Content = content,
						PostedAt = dalPost.CreationDate,
						Title = dalPost.Title,
						Settings = new PostSettingsModel()
						{
							Mode = CommentsMode.ToggleDisabled
						}
					});

				}
			}


			return View(Model);
		}


		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult Index(AddPostCommand Command)
		{
			DAL.Post dalPost = new Post();
			dalPost.CreationDate = DateTime.UtcNow;
			dalPost.Title = Command.PostTitle;
			dalPost.Text = Command.PostContent;

			using (Context)
			{
				DAL.Topic dalTopic = Context.TopicRepository.Get(Command.TopicId);
				dalTopic
					.Post
					.Add(dalPost);
			}


			return RedirectToAction("Index", new { Command.TopicId });
		}

		

	}

}
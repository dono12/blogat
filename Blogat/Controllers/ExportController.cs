﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using OfficeOpenXml;
namespace Blogat.Controllers
{
    public class ExportController : BaseController
    {
        // GET: Export
        public FileResult AllMessages()
        {
			IEnumerable<Message> dalMessages;
			MemoryStream stream;
			using (Context)
			{
				dalMessages = Context.MessageRepository.GetAll();

				using (var p = new ExcelPackage())
				{

					var ws = p.Workbook.Worksheets.Add("User messages");
					int row = 1, col = 1;

					ws.Cells[row, col].Value = "Id";
					ws.Cells[row, col + 1].Value = "Email";
					ws.Cells[row, col + 2].Value = "Nick";
					ws.Cells[row, col + 3].Value = "Phone No.";
					ws.Cells[row, col + 4].Value = "Message";
					ws.Cells[row, col + 5].Value = "Submitted at";
					row++;
					foreach (var message in dalMessages)
					{
						ws.Cells[row, col].Value = message.Id;
						ws.Cells[row, col + 1].Value = message.Email;
						if (message.UserId == null)
						{
							ws.Cells[row, col + 2].Value = message.Name;
						}
						else
						{
							ws.Cells[row, col + 2].Value = message.BlogatUser.Nick;
						}
						ws.Cells[row, col + 3].Value = message.Tel;
						ws.Cells[row, col + 4].Value = message.Text;
						ws.Cells[row, col + 5].Value = message.SubmittedAt.ToShortDateString();
						

						row++;
					}
					stream = new MemoryStream(p.GetAsByteArray());
				}
			}
			
			return File(stream.ToArray(), "application/vnd.ms-excel", "User messages.xls");
        }
    }
}
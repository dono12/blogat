﻿using Blogat.Models.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Controllers
{
    public class MessageController : BaseController
    {
		[Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
			List<MessageModel> messages = new List<MessageModel>();
			MessageViewModel Model = new MessageViewModel();
			using (Context)
			{
				IEnumerable<DAL.Message> dalMessages = Context.MessageRepository.GetAll();
				foreach(var dalMessage in dalMessages)
				{
					MessageModel message = new MessageModel();

					message.Id = dalMessage.Id;

					message.SubmittedAt = dalMessage.SubmittedAt.ToLocalTime();
					message.Name = dalMessage.Name;

					if (dalMessage.UserId != null)
					{
						message.Email = dalMessage.BlogatUser.Email;
						message.Name = dalMessage.BlogatUser.Nick;

					}
					else
					{
						message.Email = dalMessage.Email;
						message.Name = dalMessage.Name;
						message.Tel = dalMessage.Tel;
					}

					messages.Add(message);
				}
			}
			Model.Messages = messages;
			return View(Model);
        }

		[Authorize(Roles = "Admin")]
		public ActionResult Display(int MessageId)
		{
			MessageModel Model = new MessageModel();
			using (Context)
			{
				DAL.Message dalMessage = Context.MessageRepository.FirstOrDefault(x => x.Id == MessageId);
				Model.Id = dalMessage.Id;

				Model.SubmittedAt = dalMessage.SubmittedAt.ToLocalTime();
				Model.Name = dalMessage.Name;
				Model.Text = dalMessage.Text;

				if (dalMessage.UserId != null)
				{
					Model.Email = dalMessage.BlogatUser.Email;
					Model.Name = dalMessage.BlogatUser.Nick;

				}
				else
				{
					Model.Email = dalMessage.Email;
					Model.Name = dalMessage.Name;
					Model.Tel = dalMessage.Tel;
				}
				
				
			}
			return View(Model);
		}
	}

	
}

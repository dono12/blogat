﻿using Blogat.App_Start;
using Common.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Blogat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapper.Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperProfile>());
        }


		protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
		{
			if (Request.IsAuthenticated)
			{
				var Identity = new BlogatIdentity(HttpContext.Current.User.Identity);
				if (Identity.Login == null)
					FormsAuthentication.SignOut();
				HttpContext.Current.User = new BlogatPrincipal(Identity);
			}
		}
	}
}

﻿using Blogat.Models.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Dashboard
{
	public class EditTopicViewModel
	{
		public TopicModel Topic { get; set; }
		public string AllTags { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Dashboard
{
	public class RatingModel
	{
		public int TopicId { get; set; }
		public double? Rating { get; set; }
	}
}
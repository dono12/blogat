﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Models.Account
{
	public class ChangePasswordViewModel
	{
        [Required(ErrorMessage = "Te pole jest wymagane")]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", ErrorMessage = "Hasło musi się składać z minimum 8 znaków, dodatkowo z małej, dużej litery i jednej cyfry.")]
        [Display(Name = "Nowe Hasło")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Te pole jest wymagane")]
        [DataType(DataType.Password)]
        [Display(Name = "Potwierdź Nowe Hasło")]
        [Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }

        public string Guid { get; set; }

    }
}
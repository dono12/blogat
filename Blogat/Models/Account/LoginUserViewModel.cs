﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Models.Account
{
    public class LoginUserViewModel
    {
        [Required(ErrorMessage = "Nick jest wymagany.")]
        [StringLength(50)]
        [Display(Name = "Nick")]
        public string Nick { get; set; }

        [Required(ErrorMessage = "Musisz podać hasło.")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 4)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

		public string Captcha { get; set; }
    }
}
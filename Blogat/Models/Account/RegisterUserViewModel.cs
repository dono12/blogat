﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Models.Account
{
    public class RegisterUserViewModel
	{
        [StringLength(50)]
        public string Nick { get; set; }

		
		[DataType(DataType.Password)]
		public string Email { get; set; }
		
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 4)]
		[Compare("RepeatPassword")]
		public string Password { get; set; }

		
		[DataType(DataType.Password)]
		[StringLength(20, MinimumLength = 4)]
		public string RepeatPassword { get; set; }

		public string Captcha { get; set; }
	}
}
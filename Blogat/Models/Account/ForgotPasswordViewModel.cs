﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Models.Account
{
    public class ForgotPasswordViewModel
    {
        [Display(Name = "Podaj email")]
        public string Email { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Post
{
    public class CommentModel
    {
        public int? Id { set; get; }
        public string Author { set; get; }
        public string Text { set; get; }
        public DateTime? CreatedAt { set; get; }
    }
}
﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Post
{
    public class PostSettingsModel
    {
        public CommentsMode Mode { get; set; }
		public bool IsEditable { get; set; }
	}
}
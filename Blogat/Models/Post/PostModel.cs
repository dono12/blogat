﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blogat.Models.Post
{
    public class PostModel
	{
        public int PostId { get; set; }
        public string Title { get; set; }
        public MvcHtmlString Content { get; set; }
        public DateTime? PostedAt { get; set; }
        public List<CommentModel> Comments { get; set; }
        public PostSettingsModel Settings { get; set; }
    }
}
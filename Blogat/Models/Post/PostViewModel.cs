﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Post
{
	public class PostViewModel
	{
		public int TopicId { get; set; }
		public string TopicTitle { get; set; }
		public PostModel Post { get; set; }
	}
}
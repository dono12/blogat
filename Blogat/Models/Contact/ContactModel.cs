﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Contact
{
	public class ContactModel
	{
		public string Name { get; set; }
		public string Email { get; set; }
		public string Message { get; set; }
		public string Tel { get; set; }
		public bool? Success { get; set; }
	}
}
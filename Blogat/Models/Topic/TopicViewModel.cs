﻿using Blogat.Models.Post;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Topic
{
	public class TopicViewModel
	{
		public int TopicId { get; set; }
		public string TopicTitle { get; set; }
		public List<PostModel> Posts { get; set; }
	}
}
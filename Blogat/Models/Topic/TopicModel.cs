﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Models.Topic
{
    public class TopicModel
    {
        public int TopicId { get; set; }

		[Required]
        public string Title { get; set; }
		
		public DateTime CreatedAt { get; set; }

		public string LeatestActivity { get; set; }

		public double? Rating { get; set; }

		public IEnumerable<string> Tags { get; set; }

		public TopicStatus Status { get; set; }

	}
}
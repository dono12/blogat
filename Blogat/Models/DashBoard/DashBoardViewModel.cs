﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blogat.Models.Shared;
using Blogat.Models.Topic;

namespace Blogat.Models.Dashboard
{
    public class DashboardViewModel
    {
        public List<TopicModel> Topics { get; set; }
		public string AllTags { get; set; }
        public PaginationModel Pagination { get; set; }
	}
}
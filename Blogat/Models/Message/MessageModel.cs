﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Message
{
	public class MessageModel
	{
		public int Id { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string Tel { get; set; }
		public string Text { get; set; }
		public DateTime SubmittedAt { get; set; }
	}
}
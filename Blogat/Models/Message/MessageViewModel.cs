﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Message
{
	public class MessageViewModel
	{
		public List<MessageModel> Messages { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogat.Models.Shared
{
    public class PaginationModel
    {
        public int CurrentPage { get; set; }
        public int LastPage { get; set; }
    }
}
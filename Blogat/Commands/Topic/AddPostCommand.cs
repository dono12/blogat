﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Commands.Topic
{
    public class AddPostCommand
    {
        [Required]
        public int TopicId { get; set; }
        [Required]
        public string PostContent { get; set; }
        [Required]
        public string PostTitle { get; set; }
    }
}
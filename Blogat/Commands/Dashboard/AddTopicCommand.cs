﻿using Common.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Commands.Dashboard
{
    public class AddTopicCommand
    {
       [Required]
       [StringLength(Consts.MaxTitleLength, MinimumLength = Consts.MinTitleLength)]
       public string Title { get; set; }
       public string Tags { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Blogat.Commands.Contact
{
	public class AddMessageCommand
    {
        [Required]
		public string Name { get; set; }
        [Required]
        [RegularExpression("^\\w+@\\w+\\..{2,3}(.{2,3})?$", ErrorMessage = "This email adress is invalid.")]
        public string Email { get; set; }
        [Required]
        public string Message { get; set; }
        [RegularExpression("^[+]*[0-9]{9,11}$", ErrorMessage = "This phone number is invalid.")]
		public string Tel { get; set; }
	}
}
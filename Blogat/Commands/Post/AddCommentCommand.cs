﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Commands.Post
{
    public class AddCommentCommand
    {
        [Required]
        public int PostId { get; set; }
        [Required]
        public string CommentContent { get; set; }
    }
}
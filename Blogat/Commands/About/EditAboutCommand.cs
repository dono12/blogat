﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogat.Commands.About
{
	public class EditAboutCommand
    {
        [Required]
		public string Text { get; set; }
	}
}
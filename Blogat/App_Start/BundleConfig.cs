﻿using System.Web;
using System.Web.Optimization;

namespace Blogat
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/foundation/scripts").Include(
                        "~/Scripts/foundation/jquery.js",
                        "~/Scripts/foundation/what-input.js",
                        "~/Scripts/foundation/foundation.js"
                        ));


			bundles.Add(new StyleBundle("~/foundation/styles").Include(
						"~/Content/foundation/foundation.css",
						"~/dist/css/foundation-prototype.css"
						));


			bundles.Add(new ScriptBundle("~/custom/scripts").Include(
                        "~/Scripts/main.js"
                        ));



            bundles.Add(new StyleBundle("~/custom/styles").Include(
                        "~/Content/main.css",
                        "~/Content/colors.css"
                        ));


        }
    }
}

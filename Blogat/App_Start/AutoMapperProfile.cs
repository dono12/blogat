﻿using AutoMapper;
using Blogat.Models.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Extentions;
using Common.Extentions;
using Common.Enums;

namespace Blogat.App_Start
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DAL.Topic, TopicModel>()
                .ForMember(dest => dest.TopicId,
                    opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.Rating,
                    opts => opts.MapFrom(src => src.GetAvaregeRate()))
                .ForMember(dest => dest.LeatestActivity,
                    opts => opts.MapFrom(src => src.GetLatestActivity().DiffFromNow() ?? "No Activity"))
                .ForMember(dest => dest.Tags,
                    opts => opts.MapFrom(src => src.Tag.Select(x => x.TagName).OrderBy(x => x)))
                .ForMember(dest => dest.Status,
                    opts => opts.MapFrom(src => (TopicStatus)src.Status));


            
        }
    }
}
﻿
$(document).ready(function () {
	console.log("ready")
	var allTagsText = $("#all-tags")[0].innerText;
	allTags = allTagsText.split(",");
})
$(".comments-descriptor").on("click", function (ev) {
	var container =
		$(ev.target)
			.parent(".comments-container");
	var list = container
			.children(".comments-list");
			
	if (container.find(".comments-list").length === 0)
	{
		var postLink = container
				.parent(".blogat-panel")[0].dataset.location;
		document.location = postLink;
	}

    if (list.css("display") === "none")
        $(".comments-list").hide(200);
    list.toggle(200);
});
setInterval(updateRateInterval, 10000);
$(".topic-rating-star").mouseenter(hoverOnStars)
$(".topic-rating-star").mouseleave(emptyAllStars)
$(".topic-rating").mouseleave(restoreStars)
$(".topic-rating-star").on("click",TryRate)
$("input.tag-input").on("keyup", onTagInputChange)
$("input.tag-input").on("change", onTagInputChange)

var allTags = []
var currentInput = ""


function onTagInputChange()
{
	if (currentInput.length >= $(this).val().length)
	{

		currentInput = $(this).val();
		return;
	}

	currentInput = $(this).val();

	var tagList = currentInput.split(",");
	if (tagList.length > 1) {

		var tag = "";
		var i;
		$("input.tag-input").val("")
		for (i = 0; i < tagList.length; i++) {
			var text = tagList[i];
			if (text !== "") {

				$("#tag-list").append(buildTag(text));
			}
		}

		$(".tag-x").on("click", removeTag)
	}
	else
	{
		var i;
		if (tagList[0].length > 0)
		{

			for (i = 0; i < allTags.length; i++)
			{
				var listElem = allTags[i].toLowerCase();
				if (tagList[0] >= listElem.length)
					return;

				if (listElem.indexOf(tagList[0].toLowerCase()) == 0)
				{
					$(this).val(capitalizeFirstLetter(listElem))
					this.setSelectionRange(tagList[0].length, this.value.length,"forward")
				}
			}
		}
	}
}

function onTopicSubmit()
{
	var tagsInput = $(".tag-input");
	if (tagsInput[0].value.length > 0)
	{
		tagsInput[0].value += ",";
		tagsInput.keyup();
		return false;
	}

	var taglist = $("#tag-list").find(".tag-text");
	var tagTextlist = $.map(taglist, function (tag) {
		return capitalizeFirstLetter(tag.innerText.trim());
	});
	var hiddenInputValue = tagTextlist.join();
	$("#tags-hidden").val(hiddenInputValue)
}

function buildTag(text)
{
	var tagMarkup = '<div class="tag"><i class="fas fa-times tag-x"></i><span class="tag-text">';
	var closing = '</span></div>';
	return tagMarkup + text + closing;
}

function getTextFromTag(tag)
{
	var text = $(tag).children(".tag-text").text();
	return text;
}

function removeTag()
{
	var t = getTextFromTag($(this).parent());
	$(this).parent().remove();

}

function hoverOnStars()
{
	
	swapClass("fas", "far", event.target);
	var next = $(this).nextAll(".topic-rating-star");
	var prev = $(event.target).prevAll(".topic-rating-star");

	$(this).parent().children(".topic-rating-star").each(function () {
		swapClass("fa-star","fa-star-half-alt",this)
	})

	next.each(function () {
		swapClass("far", "fas", this);
	})
	prev.each(function () {
		swapClass("fas", "far", this);
	})
}

function emptyAllStars()
{
	$(this).parent().children(".topic-rating-star").each(function () {
		swapClass("far", "fas", this);
	});
}

function restoreStars()
{
	var rating = parseFloat($(this).children(".rating-value").html());
	$(this).children(".topic-rating-star").each(function()
	{
		var starVal = $(this).index();
		if (rating >= starVal) {
			swapClass("fas", "far", this);
			swapClass("fa-star", "fa-star-half-alt", this);
		}
		else if (rating >= starVal - 0.5 && rating < starVal) {

			swapClass("fas", "far", this);
			swapClass("fa-star-half-alt", "fa-star", this);
		}
		else
		{
			swapClass("far", "fas", this);
			swapClass("fa-star", "fa-star-half-alt", this);
		}
	})


}

function updateRateValue(topicId,value)
{
	
	var topicRating = $(".topic-rating[data-topic-id=" + topicId + "]").children(".rating-value");
	topicRating.html(value.toFixed(1));
	topicRating.mouseleave();

}

function updateRateInterval()
{
	var ids = "";
	var AllTopicsRatings = $(".topic-rating");
	$.each(AllTopicsRatings, function (index, value)
	{
		ids += "TopicsIds=" + $(value).data("topic-id") + "&";
	})
	$.ajax({
		url: $("#urls").data("get-ratings-url") + "?" + ids,
		type: "get",
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		success: function (response) {
			if (response.status == 200) {
				$.each(response.value, function (index, value) {
					updateRateValue(value.TopicId, value.Rating);
				})
			}
		},
		error: function (err) {
			console.log("error" + err)
		}

	})
}

//<i class="fas fa-star"></i>full
//<i class="fas fa-star-half-alt"></i> half
//<i class="far fa-star"></i> empty
function swapClass(added, removed, targetElement)
{
	var child = $(targetElement).children("i");
	if($(child).hasClass(removed))
		$(child).removeClass(removed)
	if(!$(child).hasClass(added))
		$(child).addClass(added)
}


function TryRate(value) {
	var topicId = parseInt($(this).parent(".topic-rating").attr("data-topic-id"));
	var loader = $(this).parent(".topic-rating").find("img");
	loader.removeClass("hide");
	Rate(topicId, $(this).index(),loader);
}

function Rate(topicId,value,loader) {
	$.ajax({
		url: $("#urls").data("rate-url"),
		type: "post",
		dataType: "json",
		data: JSON.stringify({ TopicId: topicId, Rate: value }),
		contentType: "application/json; charset=utf-8",
		success: function (response) {
			if (response.status == 200)
			{
				updateRateValue(topicId, response.value);
				loader.addClass("hide")
			}
			else if (response.status == 401)
			{
				document.location = $("#urls").data("login-url")
			}
		},
		error: function (err) {
			console.log("error" + err)
		}
		
	})
}

function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
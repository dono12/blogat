﻿CREATE TABLE [dbo].[Topic] (
    [Id]        INT      IDENTITY (1, 1) NOT NULL,
    [Title]     TEXT     NOT NULL,
    [Status]    INT      NOT NULL,
    [CreatedAt] DATETIME NOT NULL,
    CONSTRAINT [PK_Topic] PRIMARY KEY CLUSTERED ([Id] ASC)
);


﻿CREATE TABLE [dbo].[AboutMe] (
    [Id]   INT  NOT NULL,
    [Text] TEXT NULL,
    CONSTRAINT [PK_AboutMe] PRIMARY KEY CLUSTERED ([Id] ASC)
);


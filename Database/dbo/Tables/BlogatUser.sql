﻿CREATE TABLE [dbo].[BlogatUser] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [Nick]         VARCHAR (50)  NOT NULL,
    [Email]        VARCHAR (100) NOT NULL,
    [PasswordHash] VARCHAR (200) NOT NULL,
    [PasswordSalt] VARCHAR (20)  NOT NULL,
    [Role]         INT           NOT NULL,
    [IsActive]     BIT           NOT NULL,
    CONSTRAINT [PK_BlogatUser] PRIMARY KEY CLUSTERED ([Id] ASC)
);


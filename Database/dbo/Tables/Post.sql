﻿CREATE TABLE [dbo].[Post] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [TopicId]      INT           NOT NULL,
    [CreationDate] DATETIME      NOT NULL,
    [Title]        VARCHAR (200) NOT NULL,
    [Text]         TEXT          NOT NULL,
    CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Post_Topic] FOREIGN KEY ([TopicId]) REFERENCES [dbo].[Topic] ([Id])
);


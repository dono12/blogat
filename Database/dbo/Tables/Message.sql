﻿CREATE TABLE [dbo].[Message] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Email]       VARCHAR (100) NOT NULL,
    [Name]        VARCHAR (100) NOT NULL,
    [Tel]         VARCHAR (20)  NULL,
    [Text]        TEXT          NOT NULL,
    [SubmittedAt] DATETIME      NOT NULL,
    [UserId]      INT           NULL,
    CONSTRAINT [PK_GuestsMessage] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Message_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[BlogatUser] ([Id])
);


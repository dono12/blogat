﻿CREATE TABLE [dbo].[Comment] (
    [Id]        INT      IDENTITY (1, 1) NOT NULL,
    [PostId]    INT      NOT NULL,
    [AuthorId]  INT      NULL,
    [Text]      TEXT     NOT NULL,
    [CreatedAt] DATETIME NOT NULL,
    CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Comment_BlogatUser] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[BlogatUser] ([Id]) ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT [FK_Comment_Post] FOREIGN KEY ([PostId]) REFERENCES [dbo].[Post] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);


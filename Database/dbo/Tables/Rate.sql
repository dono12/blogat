﻿CREATE TABLE [dbo].[Rate] (
    [UserId]      INT     NOT NULL,
    [TopicId]     INT     NOT NULL,
    [TopicRating] TINYINT NOT NULL,
    CONSTRAINT [PK_Rate] PRIMARY KEY CLUSTERED ([UserId] ASC, [TopicId] ASC),
    CONSTRAINT [FK__Rate__TopicId__412EB0B6] FOREIGN KEY ([TopicId]) REFERENCES [dbo].[Topic] ([Id]),
    CONSTRAINT [FK_Topic_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[BlogatUser] ([Id])
);


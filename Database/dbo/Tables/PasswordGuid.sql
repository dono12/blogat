﻿CREATE TABLE [dbo].[PasswordGuid] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [DynamicGuid] VARCHAR (50) NOT NULL,
    [UserId]      INT          NOT NULL,
    [CreatedAt]   DATETIME     NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[BlogatUser] ([Id])
);


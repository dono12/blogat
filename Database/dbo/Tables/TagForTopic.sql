﻿CREATE TABLE [dbo].[TagForTopic] (
    [TopicId] INT NOT NULL,
    [TagId]   INT NOT NULL,
    CONSTRAINT [PK_TagForTopic] PRIMARY KEY CLUSTERED ([TopicId] ASC, [TagId] ASC),
    CONSTRAINT [FK_TagForTopic_Tag] FOREIGN KEY ([TagId]) REFERENCES [dbo].[Tag] ([Id]),
    CONSTRAINT [FK_TagForTopic_TopicId] FOREIGN KEY ([TopicId]) REFERENCES [dbo].[Topic] ([Id])
);


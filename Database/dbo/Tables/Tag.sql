﻿CREATE TABLE [dbo].[Tag] (
    [Id]      INT          IDENTITY (1, 1) NOT NULL,
    [TagName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Uniqe_name] UNIQUE NONCLUSTERED ([TagName] ASC)
);


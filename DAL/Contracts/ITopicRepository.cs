﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace DAL.IRepository
{
    public interface ITopicRepository : IRepository<Topic>
    {
		IOrderedQueryable<Topic> OrderBy<T>(Expression<Func<Topic, T>> predicate);
		IOrderedQueryable<Topic> DescendingOrderBy<T>(Expression<Func<Topic, T>> predicate);
	}
}

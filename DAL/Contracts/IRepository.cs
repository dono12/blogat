﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace DAL.IRepository
{
    public interface IRepository<T>
    {

        IQueryable<T> Include<TProperty>(Expression<Func<T, TProperty>> Path);
        IQueryable<T> Include(string Path);
        T Get(int Key);
        T First();
        int CountAll();
        int Count(Expression<Func<T, bool>> predicate);
        T First(Expression<Func<T, bool>> predicate);
        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAllByKeys(IEnumerable<int> KeyValues);
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();
        IEnumerable<T> AsEnumerable();
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
        void RemoveIf(Expression<Func<T, bool>> predicate);
        bool TryRemove(int Id);
    }
}

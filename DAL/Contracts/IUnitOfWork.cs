﻿using DAL.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contracts
{
    interface IUnitOfWork : IDisposable
    {
        IRepository<Post> PostRepository { get; set; }
        IRepository<BlogatUser> BlogatUserRepository { get; set; }
        ITopicRepository TopicRepository { get; set; }
        IRepository<Comment> CommentRepository { get; set; }
		IRepository<Message> MessageRepository { get; set; }
		IRepository<Rate> RateRepository { get; set; }
		IRepository<AboutMe> AboutMeRepository { get; set; }
		IRepository<Tag> TagRepository { get; set; }
		int Complete();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Extentions
{
	public static class IOrderedQueryableExtention
	{

		public static IEnumerable<T> GetPage<T>(this IOrderedQueryable<T> Query,  int PageNo, int PageSize)
		{
			if (PageNo < 1) throw new ArgumentOutOfRangeException();
			return Query.Skip((PageNo - 1) * PageSize).Take(PageSize).ToList();
		}
	}
}

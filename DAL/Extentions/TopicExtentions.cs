﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Extentions
{
	public static class TopicExtentions
	{
		public static double? GetAvaregeRate(this Topic topic)
		{
			IEnumerable<int> ratings = topic.Rate.Select(x => (int)x.TopicRating);
			double? rating = null;

			if (ratings.Count() > 0)
			{
				rating = ratings.Average();
				rating = Math.Floor(rating.Value * 10) / 10;

			}

			return rating;
		}

        public static DateTime? GetLatestActivity(this Topic topic)
        {

            IEnumerable<DateTime> allLatestsPostsDates =
                topic.Post.Select(x => x.CreationDate);
            

            DateTime? latestActivity = null;
            DateTime? latestPost = null;
            DateTime? latestComment = null;
            if (allLatestsPostsDates.Count() > 0)
            {

                IEnumerable<DateTime?> allLatestsCommentsDates =
                    topic.Post.Select(x => x.Comment.Count != 0 ? (DateTime?)x.Comment.Max(y => y.CreatedAt) : null);

                if (allLatestsCommentsDates != null)
                    latestComment = allLatestsCommentsDates.Max();
                latestPost = allLatestsPostsDates.Max();
                if(latestComment.HasValue )
                {
                    if (latestComment.Value > latestPost.Value)
                        latestActivity = latestComment.Value;
                    else
                        latestActivity = latestPost.Value;

                }
                else
                    latestActivity = latestPost.Value;

            }
            return latestActivity;
        }
        
	}
}

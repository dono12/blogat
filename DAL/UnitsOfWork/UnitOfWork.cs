﻿using DAL.Contracts;
using DAL.IRepository;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public IRepository<Post> PostRepository { get; set; }
        public IRepository<BlogatUser> BlogatUserRepository { get; set; }
        public ITopicRepository TopicRepository { get; set; }
        public IRepository<Comment> CommentRepository { get; set; }
		public IRepository<PasswordGuid> PasswordGuidRepository { get; set; }
		public IRepository<Message> MessageRepository { get; set; }
		public IRepository<Rate> RateRepository { get; set; }
		public IRepository<AboutMe> AboutMeRepository { get; set; }
		public IRepository<Tag> TagRepository { get; set; }

		protected readonly BlogatEntities context = new BlogatEntities();

        public UnitOfWork()
		{
			BlogatUserRepository = new Repository<BlogatUser>(context);
			TopicRepository = new TopicRepository(context);
			PostRepository = new Repository<Post>(context);
            CommentRepository = new Repository<Comment>(context);
			PasswordGuidRepository = new Repository<PasswordGuid>(context);
			MessageRepository = new Repository<Message>(context);
			RateRepository = new Repository<Rate>(context);
			AboutMeRepository = new Repository<AboutMe>(context);
			TagRepository = new Repository<Tag>(context);
		}

        public void Dispose()
        {
            Complete();
            context.Dispose();
        }

        public int Complete()
        {
            return context.SaveChanges();
        }

    }
}

﻿using DAL.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
namespace DAL.Repositories
{
    class TopicRepository : Repository<Topic>, ITopicRepository
    {

        public TopicRepository(BlogatEntities context):base(context)
        {
        }
		
		public IOrderedQueryable<Topic> DescendingOrderBy<T>(Expression<Func<Topic, T>> predicate)
        {
            return Set.OrderByDescending(predicate);
        }

		public IOrderedQueryable<Topic> OrderBy<T>(Expression<Func<Topic, T>> predicate)
        {
            return Set.OrderBy(predicate);
        }

		
	}
}

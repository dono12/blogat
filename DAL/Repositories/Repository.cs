﻿using DAL.IRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbSet<T> Set;

        public Repository(BlogatEntities context)
        {
            Set = context.Set<T>();
        }

        public void Add(T entity)
        {
            Set.Add(entity);
        }

        public IQueryable<T> Include<TProperty>(Expression<Func<T, TProperty>> Path)
        {
            return Set.Include(Path);
        }

        public IQueryable<T> Include(string Path)
        {
            return Set.Include(Path);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            Set.AddRange(entities);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return Set.Where(predicate).ToList();
        }

        public T First()
        {
            return Set.First();
        }

        public T First(Expression<Func<T, bool>> predicate)
        {
            return Set.First(predicate);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return Set.FirstOrDefault(predicate);
        }

        public T Get(int Key)
        {
            return Set.Find(Key);
        }

        public IEnumerable<T> GetAllByKeys(IEnumerable<int> KeyValues)
        {
            foreach(int key in KeyValues)
                yield return Set.Find(key);
        }

        public IEnumerable<T> GetAll()
        {
            return Set.ToList();
        }

        public void Remove(T Entity)
        {
            Set.Remove(Entity);
        }

        public void RemoveIf(Expression<Func<T, bool>> predicate)
        {
            Set.RemoveRange(Find(predicate));
        }


        public bool TryRemove(int Id)
        {
            T entity = Get(Id);
            if (entity != null)
            {
                Remove(entity);
                return true;
            }
            else
                return false;
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            Set.RemoveRange(entities);
        }

        public IEnumerable<T> AsEnumerable()
        {
            return Set.AsEnumerable();
        }

        public int CountAll()
        {
            return Set.Count();
        }


        public int Count(Expression<Func<T, bool>> predicate)
        {
            return Set.Count(predicate);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    [Flags]
    public enum UserRoles
    {
        #region MainRoles

        [Display(Name = "Standard")]
        User = 1,

		[Display(Name = "Administrator")]
		Admin = 2,

		#endregion

	}
}

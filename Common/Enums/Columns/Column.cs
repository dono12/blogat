﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums.Columns
{
    public enum Column
    {
        None,

        #region Users

        User_Login,

        User_FirstName,

        User_LastName,

        User_Email,

        #endregion

        #region Tasks
            
        Task_UserName,

        Task_Title,

        Task_CreationDate,

        Task_Description,

        Task_Status

        #endregion

    }
}

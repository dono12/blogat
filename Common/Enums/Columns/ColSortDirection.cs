﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums.Columns
{
    public enum ColSortDirection
    {
        [Display(Name = "")]
        None,

        [Display(Name = "up")]
        Asc,

        [Display(Name = "down")]
        Desc

    }
}

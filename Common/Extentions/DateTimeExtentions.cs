﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extentions
{
    public static class DateTimeExtentions
    {
        public static string DiffFromNow(this DateTime? Date)
        {
            string diff = DiffFrom(Date, DateTime.UtcNow);
            return diff != null ? diff + " ago" : null; 
        }


        public static string DiffFrom(this DateTime? Date, DateTime Target)
        {
            if (!Date.HasValue)
                return null;
            TimeSpan diff;
            if (Date.Value.CompareTo(Target) > 0)
                diff = Date.Value - Target;
            else
                diff = Target - Date.Value;

            if (diff >= TimeSpan.FromDays(365))
            {
                return diff.Days / 365 + " year" + (diff.Days / 365 > 1 ? "s" : "");
            }
            else if (diff >= TimeSpan.FromDays(30))
            {
                return diff.Days / 30 + " month" + (diff.Days / 30 > 1 ? "s" : "");
            }
            else if (diff >= TimeSpan.FromDays(1))
            {
                return diff.Days + " day" + (diff.Days > 1 ? "s" : "");
            }
            else if (diff >= TimeSpan.FromHours(1))
            {
                return diff.Hours + " hour" + (diff.Hours > 1 ? "s" : "");
            }
            else if (diff >= TimeSpan.FromMinutes(1))
            {
                return diff.Minutes + " min" + (diff.Minutes > 1 ? "s" : "");
            }
            else if (diff >= TimeSpan.FromSeconds(1))
            {
                return diff.Seconds + " sec" + (diff.Seconds > 1 ? "s" : "");
            }
            else
                return "Now";
        }
    }
}

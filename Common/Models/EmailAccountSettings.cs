﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public class EmailAccountSettings
    {
        public int Port { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Login { get; set; }
        public string From { get; set; }
        public bool EnableSsl { get; set; }

        public static EmailAccountSettings GetEmailAccountSettings()
        {
            EmailAccountSettings settings = new EmailAccountSettings();
            if (ConfigurationManager.AppSettings["SmtpPort"] != null)
                settings.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            settings.Email = ConfigurationManager.AppSettings["SmtpEmail"];
            settings.Password = ConfigurationManager.AppSettings["SmtpPassword"];
            settings.Host = ConfigurationManager.AppSettings["SmtpHost"];
            settings.From = ConfigurationManager.AppSettings["SmtpFrom"];
            settings.Login = ConfigurationManager.AppSettings["SmtpLogin"];
            if (ConfigurationManager.AppSettings["SmtpEnableSsl"] != null)
                settings.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSsl"]);
            return settings;
        }
    }
}

﻿using Common.Enums.Columns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public class ColumnSettings
    {
        public Column Type { get; set; }

        public ColSortDirection SortDirection { get; set; }

    }
}

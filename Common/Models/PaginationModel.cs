﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Common.Models
{
    public class PaginationModel
    {
        public RouteValueDictionary Params { get; set; }
        public bool IsNextAvailable { get; set; }
        public bool IsPreviousAvailable { get; set; }
        int TotalItemsCount { get; set; }
        public int CurrentPage { get; set; }
    }
}

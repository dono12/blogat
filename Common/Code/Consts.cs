﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Code
{
    public class Consts
    {
        public const string SessionExpirationTime = "sessionExpirationTime";
        public const string SessionCodeVerified = "sessionCodeVerified";
        public const int PostPreviewLength = 120;
		public const int MinTitleLength = 4;
		public const int MaxTitleLength = 80;
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using DAL;
using System.Web.Security;
using Common.Models;

namespace Common.Helpers
{ 
    public class PasswordHelper
	{
		/// <summary>
		/// Minimum length of user-created password
		/// </summary>
		public static int PasswordPolicyMinChars = 8;

		/// <summary>
		/// Maximum length of user-created password
		/// </summary>
		public static int PasswordPolicyMaxChars = 20;


        /// <summary>
        /// length of user-generated password
        /// </summary>
        public static int RandomPasswordLength = 10;


        /// <summary>
        /// Compares hash password with specified hash
        /// </summary>
        public static bool CheckHash( string passwordToCheck, string hashString, string salt )
		{
			string originalHash = GetHash( passwordToCheck, salt );
			return ( originalHash == hashString );
		}

		/// <summary>
		/// Generates hash for specified text
		/// </summary>
		public static string GetHash( string password, string salt )
		{
			string textToHash = string.Format( "{0}{1}",
				password != null ? password : string.Empty,
				salt != null ? salt : string.Empty );

			UnicodeEncoding ue = new UnicodeEncoding();
			byte[] hashValue;
			byte[] message = ue.GetBytes( textToHash );

			SHA512Managed hashString = new SHA512Managed();
			string hex = string.Empty;

			hashValue = hashString.ComputeHash( message );
			foreach ( byte x in hashValue )
			{
				hex += String.Format( "{0:x2}", x );
			}
			return hex;
		}

		/// <summary>
		/// Generates random 8-char salt
		/// </summary>
		public static string GenerateSalt()
		{
			return Guid.NewGuid().ToString( "N" ).Substring( 0, 8 );
		}
        public static HashedModel GenerateHashWithSalt(string Password)
        {
            string salt = GenerateSalt();
            string passwordHash = GetHash(Password, salt);

            var Model = new HashedModel();
            Model.Hash = passwordHash;
            Model.Salt = salt;

            return Model;
        }

        public static string GenereteRandomPassword()
        {
            return Membership.GeneratePassword(RandomPasswordLength,0);
        }

		public static bool Validate( string password, string username = null )
		{
			string errorMessage = string.Empty;
			return Validate( password, out errorMessage, username );
		}

		/// <summary>
		/// Validates specified password against current security policy rules.
		/// If username is also provided, the password is checked whether it doesn't contain it (case-insensitive).
		/// </summary>
		/// <returns>True if password complies with policy</returns>
		public static bool Validate( string password, out string errorMessage, string username = null )
		{
			errorMessage = "Password policy rules not met";
			// must not be empty
			if ( string.IsNullOrEmpty( password ) )
				return false;

			// length must be within [MinChars; MaxChars]
			if ( password.Length < PasswordPolicyMinChars || PasswordPolicyMaxChars < password.Length )
			{
				errorMessage = string.Format( "Must consist of at least {0} and maximum of {1} characters", PasswordPolicyMinChars, PasswordPolicyMaxChars );
				return false;
			}
				

			// must contain at least 1 letter
			if ( !password.Any( ch => char.IsLetter( ch ) ) )
			{
				errorMessage = "Must contain at least 1 letter";
				return false;
			}
				

			// must contain at least 1 digit
			if ( !password.Any( ch => char.IsDigit( ch ) ) )
			{
				errorMessage = "Must contain at least 1 digit";
				return false;
			}
				

			// must not contain any whitespaces
			if ( password.Any( ch => char.IsWhiteSpace( ch ) ) )
			{
				errorMessage = "Must not contain white spaces";
				return false;
			}

			// must not contain 3 or more same characters in a row
			if ( password.Where( ( ch, i ) => i >= 2 && password[ i - 1 ] == ch && password[ i - 2 ] == ch ).Any() )
			{
				errorMessage = "Must not contain 3 or more same characters in a row";
				return false;
			}
				

			// must not start or end with special character
			char first = password[ 0 ];
			char last = password[ password.Length - 1 ];
			if ( !( ( char.IsDigit( first ) || char.IsLetter( first ) ) && ( char.IsDigit( last ) || char.IsLetter( last ) ) ) )
			{
				errorMessage = "Must not start or end with special character";
				return false;
			}
				

			// must not contain username (if provided)
			if ( !string.IsNullOrEmpty( username ) && password.ToLower().Contains( username.ToLower() ) )
			{
				errorMessage = "Must not contain username";
				return false;
			}
				
			// otherwise complies with the policy
			return true;
		}

		
	}
}
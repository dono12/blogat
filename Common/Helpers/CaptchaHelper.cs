﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Common.Helpers
{
	public static class CaptchaHelper
	{
		

		public static MemoryStream GetCaptchaImage(string Text)
		{
			Image img = new Bitmap(1, 1);
			Graphics drawing = Graphics.FromImage(img);

			Font font = new Font("Arial", 15);
			SizeF textSize = drawing.MeasureString(Text, font);
			
			img.Dispose();
			drawing.Dispose();
			int width = (int)textSize.Width + 40,
				height = (int)textSize.Height + 20;

			
			img = new Bitmap(width, height);
			drawing = Graphics.FromImage(img);
			

			Color backColor = Color.SeaShell;
			Random r = new Random();

			Color textColor = Color.FromKnownColor(KnownColor.DarkOrange);
			drawing.Clear(backColor);
			
			Brush textBrush = new SolidBrush(textColor);


			drawing.DrawString(Text, font, textBrush, 20, 10);


			Pen pen = new Pen(Color.White);
			int red, green, blue, alpha;
			Point start, end, ellipsePoint;
			Size ellipseSize;
			Rectangle rectangle;

			for (int i = 0; i < 500; i++)
			{
				start = new Point(r.Next(width), r.Next(height));
				end = new Point(r.Next(width), r.Next(height));
				ellipsePoint = new Point(r.Next(width), r.Next(height));
				ellipseSize = new Size((int)r.Next(width) / 2, (int)r.Next(width) / 2);
				rectangle = new Rectangle(ellipsePoint, ellipseSize);
				red = r.Next(255);
				green = r.Next(255);
				blue = r.Next(255);
				alpha = r.Next(30)+5;

				pen.Color = Color.FromArgb(alpha, red, green, blue);
				drawing.DrawLine(pen, start, end);
				drawing.DrawEllipse(pen, rectangle);
			}

			drawing.Save();


			font.Dispose();
			textBrush.Dispose();
			drawing.Dispose();

			MemoryStream memoryStream = new MemoryStream();
			img.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
			img.Dispose();

			return memoryStream;
		}

		
	}
}

﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers.Smtp
{
    public class SmtpProvider
    {
        public SmtpClient SmtpClient;
        public EmailAccountSettings Settings;

        public SmtpProvider()
        {
            SmtpClient = new SmtpClient();
            Settings = EmailAccountSettings.GetEmailAccountSettings();
            SmtpClient.Port = Settings.Port;
            SmtpClient.Host = Settings.Host;
            SmtpClient.EnableSsl = Settings.EnableSsl;
            SmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpClient.UseDefaultCredentials = false;
            SmtpClient.Credentials = new System.Net.NetworkCredential(Settings.Login, Settings.Password);
        }

        
    }
}

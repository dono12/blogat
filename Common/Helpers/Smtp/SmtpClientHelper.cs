﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers.Smtp
{
    public static class SmtpClientHelper
    {


        public static void SendEmail(string Messege, string Subject, IEnumerable<string> TargetAdresses)
        {
            SmtpProvider provider = new SmtpProvider();
            SmtpClient client = provider.SmtpClient;
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(provider.Settings.From);

            foreach(var adress in TargetAdresses)
                mail.To.Add(adress);

            mail.Body = Messege;
            mail.IsBodyHtml = true;
            mail.Subject = Subject;

            client.Send(mail);
        }

        public static string GetDefaultTemplate(string Messege)
        {
            return Messege;
        }

        private static EmailAccountSettings GetEmailSettings()
        {
            return EmailAccountSettings.GetEmailAccountSettings();
        }
    }
}

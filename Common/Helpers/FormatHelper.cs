﻿using Common.Code;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public static class FormatHelper
    {
        public static string ShortenText(string Sentence,int LetterLength,bool CutWords = false)
        {
            string shortSentence = "";
            string[] words = Sentence.Split(' ');

            if (Sentence.Length < LetterLength)
                return Sentence;
            if (CutWords)
            {
                shortSentence = shortSentence.Substring(0, LetterLength);
            }
            else
            {
                foreach (var word in words)
                {
                    if (shortSentence.Length > LetterLength)
                        break;
                    shortSentence += " " + word;
                }
            }

            return shortSentence;
        }

        public static PaginationModel GetPaginationModel(int AllItemsCount, int PageNo, int PageSize = 10)
        {
            PaginationModel Model = new PaginationModel();

            Model.CurrentPage = PageNo;
            int begin = (PageNo - 1) * PageSize,
               count = PageSize;

            Model.IsNextAvailable = begin + count < AllItemsCount;
            Model.IsPreviousAvailable = begin > 0 && begin - count <= AllItemsCount;
            
            
            return Model;
        }

        public static List<T> GetPageItems<T>(List<T> AllItems, int PageNo, int PageSize = 10)
        {
            int begin = (PageNo - 1) * PageSize,
               count = PageSize,
               allCount = AllItems.Count;
            if (count < 0)
                count = 0;
            if (begin < 0)
                begin = 0;
            if (count > allCount)
                count = allCount;

            if (count > allCount - begin)
                count = allCount - begin;

            try
            {
                return AllItems.GetRange(begin, count);
            }
            catch
            {
                return new List<T>();
            }

        }
    }
}

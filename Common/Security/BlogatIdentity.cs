﻿using Common.Enums;
using DAL;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Common.Security
{
    public class BlogatIdentity : IIdentity
    {
        IIdentity Identity;

        public int Id { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UserRoles RolesFlags { get; set; }

        public BlogatIdentity(IIdentity Identity)
        {
            this.Identity = Identity;
            BlogatUser user;
            using (var context = new UnitOfWork())
            {
                user = context.BlogatUserRepository.FirstOrDefault(x => x.Nick == Identity.Name);
            }
            if(user != null)
            {
                Id = user.Id;
                Login = Name;
                FirstName = user.Nick;
                Email = user.Email;
                RolesFlags = (UserRoles)user.Role;
            }

        }

        public string Name
        {
            get { return Identity.Name; }
        }

        public string AuthenticationType
        {
            get { return Identity.AuthenticationType; }
        }

        public bool IsAuthenticated
        {
            get { return Identity.IsAuthenticated; }
        }
    }
}

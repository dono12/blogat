﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
namespace Common.Security
{
    public class BlogatPrincipal : IPrincipal
    {
        public IIdentity Identity { get; }

        public bool IsInRole(string role)
        {
            BlogatIdentity identity = (BlogatIdentity)Identity;
            UserRoles UserRoles = (UserRoles)Enum.Parse(typeof(UserRoles), role, true);

            return (identity.RolesFlags & UserRoles) != 0;
        }

        public BlogatPrincipal(IIdentity Identity)
        {
            this.Identity = Identity;
        }
    }
}
